from functools import partial
import inspect
from itertools import count
import logging
import types
import typing as ty

from outcome import Value, Error
import trio
from trio.abc import Channel, SendChannel, ReceiveChannel

from jeepney.auth import SASLParser, make_auth_external, BEGIN, AuthenticationError
from jeepney.bus import get_bus
from jeepney.low_level import Parser, MessageType, Message, HeaderFields
from jeepney.wrappers import ProxyBase, MessageGenerator
from jeepney.bus_messages import MatchRule, message_bus
from jeepney.routing import Router, _SubscriptionHandle, MessageBodyTy

try:  #PY37+
    from contextlib import AsyncExitStack
except ImportError:  #PY36
    from async_exit_stack import AsyncExitStack

log = logging.getLogger(__name__)

__all__ = [
    'open_dbus_connection',
    'open_dbus_requester',
    'Proxy',
]


T = ty.TypeVar("T")


class DBusConnection(Channel):
    """A 'plain' D-Bus connection with no matching of replies.

    This doesn't run any separate tasks: sending and receiving are done in
    the task that calls those methods. It's suitable for easily implementing
    servers: several worker tasks can receive requests and send replies.
    For a typical client pattern, see DBusRequester.

    Implements trio's channel interface for Message objects.
    """
    def __init__(self, socket: trio.SocketStream):
        self.socket = socket
        self.parser = Parser()
        self.outgoing_serial = count(start=1)
        self.unique_name = None
        self.send_lock = trio.Lock()

    async def send(self, message: Message, *, serial: ty.Optional[int] = None):
        async with self.send_lock:
            if serial is None:
                serial = next(self.outgoing_serial)
            await self.socket.send_all(message.serialise(serial=serial))

    async def receive(self) -> Message:
        while True:
            msg = self.parser.get_next_message()
            if msg is not None:
                return msg

            b = await self.socket.receive_some()
            if not b:
                raise trio.EndOfChannel("Socket closed at the other end")
            self.parser.add_data(b)

    async def aclose(self):
        await self.socket.aclose()

    def requester(self):
        """Temporarily wrap this connection as a DBusRequester

        To be used like::

            async with conn.requester() as req:
                reply = await req.send_and_get_reply(msg)

        or::

            async with conn.requester() as req:
                message_bus = Proxy(jeepney.bus_messages.message_bus, req)
                reply = await message_bus.Ping()

        While the requester is running, you shouldn't use :meth:`receive`.
        Once the requester is closed, you can use the plain connection again.
        """
        return DBusRequester(self)


async def connect_and_authenticate(bus: str = 'SESSION') -> DBusConnection:
    """Open a 'plain' D-Bus connection, with no new tasks"""
    bus_addr = get_bus(bus)
    sock: trio.SocketStream = await trio.open_unix_socket(bus_addr)

    # Authentication flow
    await sock.send_all(b'\0' + make_auth_external())
    auth_parser = SASLParser()
    while not auth_parser.authenticated:
        b = await sock.receive_some()
        auth_parser.feed(b)
        if auth_parser.error:
            raise AuthenticationError(auth_parser.error)

    await sock.send_all(BEGIN)
    # Authentication finished

    conn = DBusConnection(sock)
    conn.parser.add_data(auth_parser.buffer)

    # Say *Hello* to the message bus - this must be the first message, and the
    # reply gives us our unique name.
    async with conn.requester() as requester:
        reply = await requester.send_and_get_reply(message_bus.Hello())
        conn.unique_name = reply[0]

    return conn


class Future:
    """A very simple Future for trio based on `trio.Event`."""
    def __init__(self):
        self._outcome = None
        self._event = trio.Event()

    def set_result(self, result):
        self._outcome = Value(result)
        self._event.set()

    def set_exception(self, exc):
        self._outcome = Error(exc)
        self._event.set()

    async def get(self):
        await self._event.wait()
        return self._outcome.unwrap()


CloseNotifyTy = ty.Callable[[], ty.Coroutine[ty.Any, ty.Any, None]]


class MemoryReceiveChannelWrapper(ReceiveChannel[T], ty.Generic[T]):
    """Wrapper around a `trio.MemoryReceiveChannel` that invokes the given
    *close_notify* function once the last clone of this receive channel closes."""
    __slots__ = ("_inner", "_close_notify")
    _inner: trio.MemoryReceiveChannel
    _close_notify: ty.Optional[CloseNotifyTy]

    def __init__(self, inner: ReceiveChannel[T], close_notify: CloseNotifyTy):
        self._inner = ty.cast(trio.MemoryReceiveChannel, inner)
        self._close_notify = close_notify

    def clone(self) -> "MemoryReceiveChannelWrapper":
        return MemoryReceiveChannelWrapper(self._inner, self._close_notify)

    async def receive(self) -> T:
        return await self._inner.receive()

    def receive_nowait(self) -> T:
        return self._inner.receive_nowait()

    def statistics(self) -> "trio._channel.MemoryChannelStats":
        return self._inner.statistics()

    async def aclose(self) -> None:
        if self._close_notify is not None:
            last_recv_channel = (self._inner.statistics().open_receive_channels == 1)
            close_notify, self._close_notify = self._close_notify, None
            try:
                await self._inner.aclose()
            finally:
                await close_notify()


ExceptionTy = ty.TypeVar("ExceptionTy", bound=BaseException)
_DBusRequesterTy = ty.TypeVar("_DBusRequesterTy", bound="DBusRequester")
_RouterDispatchTy = ty.Union[
    SendChannel[MessageBodyTy],
    ty.Callable[[MessageBodyTy], None],
    ty.Callable[[MessageBodyTy], ty.Coroutine[ty.Any, ty.Any, None]],
]

class DBusRequester:
    """A 'client' D-Bus connection which can wait for a specific reply.

    This runs a background receiver task, and makes it possible to send a
    request and wait for the relevant reply.
    """
    __slots__ = (
        "_nursery_mgr", "_send_cancel_scope", "_rcv_cancel_scope",
        "_conn", "_to_send", "_to_be_sent",
        "_send_channels", "_match_rules", "_subscription_handles",
        "router",
    )
    _nursery_mgr: ty.Optional["trio._core._run.NurseryManager"]
    _send_cancel_scope: ty.Optional[trio.CancelScope]
    _rcv_cancel_scope: ty.Optional[trio.CancelScope]

    _conn: DBusConnection
    _to_send: trio.MemorySendChannel
    _to_be_sent: trio.MemoryReceiveChannel

    _send_channels: ty.List[trio.MemorySendChannel]
    _match_rules: ty.List[MatchRule]
    _subscription_handles: ty.List[_SubscriptionHandle]

    router: ty.Optional[Router[_RouterDispatchTy]]


    def __init__(self, conn: DBusConnection) -> None:
        self._nursery_mgr = None
        self._send_cancel_scope = None
        self._rcv_cancel_scope = None

        self._conn = conn
        self._to_send, self._to_be_sent = trio.open_memory_channel(0)

        self._send_channels = []
        self._match_rules = []
        self._subscription_handles = []

        self.router = None

    @property
    def unique_name(self) -> str:
        return self._conn.unique_name

    async def send(self, message: Message) -> None:
        if self.router is None:
            raise RuntimeError("Receiver task is not running")

        serial, _ = self.router.outgoing(message)

        # Hand off the actual sending to a separate task. This ensures that
        # cancelling the task that makes a D-Bus message can't break the
        # connection by sending an incomplete message.
        await self._to_send.send(message.serialise(serial=serial))

    async def send_and_get_reply(self, message: Message) -> MessageBodyTy:
        """Send a method call message and wait for the reply

        Returns the reply message (method return or error message type).
        """
        if self.router is None:
            raise RuntimeError("Receiver task is not running")

        serial, handle = self.router.outgoing(message, expect_reply=True)

        # Hand off the actual sending to a separate task. This ensures that
        # cancelling the task that makes a D-Bus message can't break the
        # connection by sending an incomplete message.
        await self._to_send.send(message.serialise(serial=serial))

        return (await handle.get())

    async def receive_signals(self, match_rule: MatchRule) \
          -> MemoryReceiveChannelWrapper[MessageBodyTy]:
        """Subscribe to signal on the bus and return a `trio.MemoryReceiveChannel`-
        like channel of matching signal messages.

        Additionally to managing the actual subscription this method will also
        ensure that the given *match_rule* is actually subscribed to one the
        connected bus before returning. The subscription will automatically be
        canceled once the last clone of the returned receive channel is closed.
        """
        if self.router is None:
            raise RuntimeError("Receiver task is not running")

        exit_stack = AsyncExitStack()
        try:
            send_channel, recv_channel = trio.open_memory_channel(1)
            send_channel = await exit_stack.enter_async_context(send_channel)

            self._send_channels.append(send_channel)
            exit_stack.callback(self._send_channels.remove, send_channel)
            self._match_rules.append(match_rule)
            exit_stack.callback(self._match_rules.remove, match_rule)

            handle = exit_stack.enter_context(
                self.router.subscribe_signal_rule(send_channel, match_rule)
            )
            self._subscription_handles.append(handle)
            exit_stack.callback(self._subscription_handles.remove, handle)

            message_bus_proxy = Proxy(message_bus, self)
            await message_bus_proxy.AddMatch(match_rule)
            exit_stack.push_async_callback(message_bus_proxy.RemoveMatch, match_rule)

            return MemoryReceiveChannelWrapper(recv_channel, exit_stack.aclose)
        except:
            await exit_stack.aclose()
            raise

    # Task management -------------------------------------------

    async def start(self, nursery: trio.Nursery) -> None:
        if self.router is not None:
            raise RuntimeError("DBusRequester tasks are already running")
        self._send_cancel_scope = await nursery.start(self._sender)
        self._rcv_cancel_scope = await nursery.start(self._receiver, nursery)

    async def aclose(self) -> None:
        """Stop the sender & receiver tasks"""
        if self._send_cancel_scope is not None:
            self._send_cancel_scope.cancel()
            self._send_cancel_scope = None
        if self._rcv_cancel_scope is not None:
            self._rcv_cancel_scope.cancel()
            self._rcv_cancel_scope = None

    async def __aenter__(self: _DBusRequesterTy) -> _DBusRequesterTy:
        self._nursery_mgr = trio.open_nursery()
        nursery = await self._nursery_mgr.__aenter__()
        await self.start(nursery)
        return self

    async def __aexit__(self,
        exc_type: ty.Optional[ty.Type[ExceptionTy]],
        exc_val: ty.Optional[ExceptionTy],
        exc_tb: ty.Optional[types.TracebackType],
    ) -> bool:
        await self.aclose()
        result = await self._nursery_mgr.__aexit__(exc_type, exc_val, exc_tb)
        self._nursery_mgr = None
        return result

    # Code to run in sender task --------------------------------------

    async def _sender(self, *, task_status=trio.TASK_STATUS_IGNORED) -> None:
        with trio.CancelScope() as cscope:
            task_status.started(cscope)
            async for bmsg in self._to_be_sent:
                async with self._conn.send_lock:
                    await self._conn.socket.send_all(bmsg)

    # Code to run in receiver task ------------------------------------

    def _handle_unknown(self, msg: Message) -> None:
        """Handle one received message"""
        log.debug("Discarded unknown message: %s", msg)

    def _dispatch_to_task(self, nursery: trio.Nursery, func: _RouterDispatchTy, *args) -> None:
        """Handle one received signal or method call message"""
        if isinstance(func, SendChannel):
            args = args[0] if len(args) == 1 else args
            try:
                func.send_nowait(args)
            except trio.WouldBlock:
                nursery.start_soon(func.send, args)
        elif inspect.iscoroutinefunction(func):
            nursery.start_soon(func, *args)
        else:
            # Synchronous functions are always supposed to be
            # “fast” / non-blocking in Trio land
            func(*args)

    async def _receiver(self, nursery: trio.Nursery, *, task_status=trio.TASK_STATUS_IGNORED) -> None:
        """Receiver loop - runs in a separate task"""
        with trio.CancelScope() as cscope:
            self.router = Router(
                Future, serial_counter=self._conn.outgoing_serial,
                dispatch_cb=partial(self._dispatch_to_task, nursery),
                unknown_cb=self._handle_unknown,
            )
            task_status.started(cscope)
            try:
                while True:
                    msg = await self._conn.receive()
                    self.router.incoming(msg)
            finally:
                # Prevent further use of the router (but don't kill it yet)
                router, self.router = self.router, None
                try:
                    # Try to close all open subscription handles
                    subscription_handles, self._subscription_handles = \
                        self._subscription_handles, []
                    for subscription_handle in subscription_handles:
                        try:
                            subscription_handle.close()
                        except:
                            pass

                    # Try to remove all active match rules
                    message_bus_proxy = Proxy(message_bus, self)
                    match_rules, self._match_rules = self._match_rules, []
                    for match_rule in match_rules:
                        try:
                            await message_bus_proxy.RemoveMatch(match_rule)
                        except:
                            pass

                    # Try to close all open send channels
                    send_channels, self._send_channels = self._send_channels, []
                    for send_channel in send_channels:
                        try:
                            await send_channel.aclose()
                        except:
                            pass
                finally:
                    # Now kill the router as well
                    router.abort()


class Proxy(ProxyBase):
    def __init__(self, msggen: MessageGenerator, requester: DBusRequester) -> None:
        super().__init__(msggen)
        if not isinstance(requester, DBusRequester):
            raise TypeError("Proxy can only be used with DBusRequester")
        self._requester = requester

    def _method_call(self, make_msg: ty.Callable[..., Message]) \
        -> ty.Callable[..., ty.Coroutine[ty.Any, ty.Any, MessageBodyTy]]:
        async def inner(*args, **kwargs) -> MessageBodyTy:
            msg = make_msg(*args, **kwargs)
            assert msg.header.message_type is MessageType.method_call
            return await self._requester.send_and_get_reply(msg)

        return inner


class _ClientConnectionContext:
    __slots__ = ("bus", "conn", "req_ctx")
    bus: str
    conn: DBusConnection
    req_ctx: DBusRequester

    def __init__(self, bus: str = 'SESSION') -> None:
        self.bus = bus

    async def __aenter__(self) -> DBusRequester:
        self.conn = await connect_and_authenticate(self.bus)
        try:
            self.req_ctx = self.conn.requester()
            return await self.req_ctx.__aenter__()
        except:
            await self.conn.aclose()
            raise

    async def __aexit__(self,
        exc_type: ty.Optional[ty.Type[ExceptionTy]],
        exc_val: ty.Optional[ExceptionTy],
        exc_tb: ty.Optional[types.TracebackType],
    ) -> bool:
        try:
            return await self.req_ctx.__aexit__(exc_type, exc_val, exc_tb)
        finally:
            await self.conn.aclose()


def open_dbus_requester(bus: str = 'SESSION') -> _ClientConnectionContext:
    """Open a 'client' D-Bus connection with a receiver task.

    Use as an async context manager::

        async with open_requester() as req:
            ...

    This is a shortcut for::

        async with await connect_and_authenticate() as conn:
            async with conn.requester() as req:
                ...
    """
    return _ClientConnectionContext(bus)
