import trio
import pytest

from jeepney import DBusAddress, MessageType, new_method_call, new_signal
from jeepney.bus_messages import MatchRule, message_bus
from jeepney.integrate.trio import (
    connect_and_authenticate, open_dbus_requester, Proxy,
)
from .utils import have_session_bus

pytestmark = [
    pytest.mark.trio,
    pytest.mark.skipif(
        not have_session_bus, reason="Tests require DBus session bus"
    ),
]

# Can't use any async fixtures here, because pytest-asyncio tries to handle
# all of them: https://github.com/pytest-dev/pytest-asyncio/issues/124

async def test_connect():
    conn = await connect_and_authenticate(bus='SESSION')
    async with conn:
        assert conn.unique_name.startswith(':')

bus_peer = DBusAddress(
    bus_name='org.freedesktop.DBus',
    object_path='/org/freedesktop/DBus',
    interface='org.freedesktop.DBus.Peer'
)

async def test_send_and_get_reply():
    ping_call = new_method_call(bus_peer, 'Ping')
    async with open_dbus_requester(bus='SESSION') as req:
        with trio.fail_after(5):
            reply = await req.send_and_get_reply(ping_call)

    assert reply == ()

async def test_proxy():
    async with open_dbus_requester(bus='SESSION') as req:
        proxy = Proxy(message_bus, req)
        name = "io.gitlab.takluyver.jeepney.examples.Server"
        res = await proxy.RequestName(name)
        assert res in {(1,), (2,)}  # 1: got the name, 2: queued

        has_owner, = await proxy.NameHasOwner(name)
        assert has_owner is True

async def test_receive_signals():
    with trio.fail_after(5):
        async with open_dbus_requester(bus='SESSION') as req1, \
                   open_dbus_requester(bus='SESSION') as req2:
            req1_addr = DBusAddress('/', req1.unique_name, "io.gitlab.takluyver.jeepney.examples.Server")
            
            match_rule = MatchRule(
                type      = "signal",
			    sender    = req1_addr.bus_name,
			    interface = req1_addr.interface,
			    path      = req1_addr.object_path,
			    member    = "signalMsg",
            )
            async with await req2.receive_signals(match_rule) as signal_channel:
                with pytest.raises(trio.WouldBlock):
                    signal_channel.receive_nowait()

                await req1.send(new_signal(req1_addr, "signalMsg", "s", ("Hallo World",)))

                assert await signal_channel.receive() == ("Hallo World",)

                with pytest.raises(trio.WouldBlock):
                    signal_channel.receive_nowait()