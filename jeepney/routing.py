import itertools
import types
import typing as ty

from .bus_messages import MatchRule
from .low_level import Message, MessageType, HeaderFields
from .wrappers import DBusErrorResponse


F = ty.TypeVar("F")
K = ty.TypeVar("K")
T = ty.TypeVar("T")
if hasattr(ty, "Literal"):  #PY38+
    ty_Literal_False = ty.Literal[False]
else:  #PY37-
    ty_Literal_False = bool


class NoReplyError(Exception):
    pass


ExceptionTy = ty.TypeVar("ExceptionTy", bound=BaseException)
_SubscriptionHandleTy = ty.TypeVar("_SubscriptionHandleTy", bound="_SubscriptionHandle")


class _SubscriptionHandle(ty.Generic[K, T]):
    __slots__ = ("_map", "_key", "_item")
    _map: ty.Optional[ty.Dict[K, T]]
    _key: ty.Optional[K]
    _item: ty.Optional[T]

    def __init__(self, map: ty.Dict[K, T], key: K, item: T) -> None:
        self._map  = map
        self._key  = key
        self._item = item

    def close(self) -> None:
        if self._map is not None:
            assert self._key is not None and self._item is not None
            items = self._map[self._key]
            items.remove(self._item)
            if len(items) < 1:
                del self._map[self._key]

        self._map = self._key = self._item = None

    def __enter__(self: _SubscriptionHandleTy) -> _SubscriptionHandleTy:
        return self

    def __exit__(self,
        exc_type: ty.Optional[ty.Type[ExceptionTy]],
        exc_val: ty.Optional[ExceptionTy],
        exc_tb: ty.Optional[types.TracebackType],
    ) -> ty_Literal_False:
        self.close()
        return False


MessageBodyTy = ty.Tuple[ty.Any, ...]
DispatchCbTy = ty.Callable[[F, MessageBodyTy], None]
UnknownCbTy = ty.Callable[[Message], None]

if hasattr(ty, "Protocol"):  #PY38+
    class HandleProto(ty.Protocol):
        def set_exception(self, exc: BaseException) -> None:
            ...

        def set_result(self, body: MessageBodyTy) -> None:
            ...
else:  #PY37-
    HandleProto = ty.Any


class Router(ty.Generic[F]):
    """Routing for messages coming back to a client application.
    :param handle_factory: Constructor for an object like asyncio.Future,
        with methods *set_result* and *set_exception*. Outgoing method call
        messages will get a handle associated with them.
    :param on_unhandled: Callback for messages not otherwise dispatched.
    """
    __slots__ = (
        "_reply_handles", "_signal_callbacks", "_method_callbacks",
        "_handle_factory", "_serial_counter",
        "_dispatch_cb", "_unknown_cb",
    )
    _reply_handles: ty.Dict[int, HandleProto]
    _signal_callbacks: ty.Dict[ty.Tuple[str, str, str], ty.List[F]]
    _method_callbacks: ty.Dict[ty.Tuple[str, str, str], ty.List[F]]

    _handle_factory: ty.Type[HandleProto]
    _serial_counter: ty.Iterator[int]

    _dispatch_cb: DispatchCbTy[F]
    _unknown_cb: UnknownCbTy

    DEFAULT_DISPATCH_CB: DispatchCbTy[ty.Callable[[MessageBodyTy], None]] = \
        staticmethod(lambda f, *a: f(*a))
    DEFAULT_UNKNOWN_CB: UnknownCbTy = staticmethod(lambda m: None)

    def __init__(self,
            handle_factory: ty.Type[HandleProto], *,
            serial_counter: ty.Optional[ty.Iterator[int]] = None,
            dispatch_cb: ty.Optional[DispatchCbTy[F]] = None,
            unknown_cb: ty.Optional[UnknownCbTy] = None,
    ):
        self._reply_handles    = {}
        self._signal_callbacks = {}
        self._method_callbacks = {}

        self._handle_factory = handle_factory
        self._serial_counter = serial_counter or itertools.count(start=1)

        self._dispatch_cb = dispatch_cb or self.DEFAULT_DISPATCH_CB
        self._unknown_cb  = unknown_cb  or self.DEFAULT_UNKNOWN_CB

    def abort(self):
        handles, self._reply_handles = self._reply_handles, {}
        for handle in handles.values():
            handle.set_exception(NoReplyError("Reply receiver stopped"))

    def outgoing(self, msg: Message, *, expect_reply: bool = False):
        """Set the serial number in the message & make a handle if a method call
        """
        serial = next(self._serial_counter)

        if expect_reply:
            if msg.header.message_type is not MessageType.method_call:
                raise TypeError("Only method call messages have replies")
            self._reply_handles[serial] = handle = self._handle_factory()
            return serial, handle
        else:
            return serial, None

    def subscribe_signal(
            self, callback: F, path: str, interface: str, member: str
    ) -> _SubscriptionHandle:
        """Add a callback for a signal.
        """
        key = (path, interface, member)
        self._signal_callbacks.setdefault(key, []).append(callback)
        return _SubscriptionHandle(self._signal_callbacks, key, callback)

    def subscribe_signal_rule(
            self, callback: F, match_rule: MatchRule
    ) -> _SubscriptionHandle:
        """Add a callback for a signal.
        """
        return self.subscribe_signal(
            callback,
            match_rule.header_fields["path"],
            match_rule.header_fields["interface"],
            match_rule.header_fields["member"],
        )

    def subscribe_method(
            self, callback: F, path: str, interface: str, member: str
    ) -> _SubscriptionHandle:
        """Add a callback for an incoming method call.
        """
        key = (path, interface, member)
        self._method_callbacks.setdefault(key, []).append(callback)
        return _SubscriptionHandle(self._method_callbacks, key, callback)

    def subscribe_method_rule(
            self, callback: F, match_rule: MatchRule
    ) -> _SubscriptionHandle:
        """Add a callback for an incoming method call.
        """
        return self.subscribe_method(
            callback,
            match_rule.header_fields["path"],
            match_rule.header_fields["interface"],
            match_rule.header_fields["member"],
        )

    def incoming(self, msg: Message) -> None:
        """Route an incoming message.
        """
        hdr = msg.header

        # Signals:
        if hdr.message_type is MessageType.signal:
            key = (hdr.fields.get(HeaderFields.path, None),
                   hdr.fields.get(HeaderFields.interface, None),
                   hdr.fields.get(HeaderFields.member, None)
                  )
            callbacks = self._signal_callbacks.get(key, None)
            if callbacks is not None:
                for cb in callbacks:
                    self._dispatch_cb(cb, msg.body)
                return

        # Incoming method calls:
        if hdr.message_type is MessageType.method_call:
            key = (hdr.fields.get(HeaderFields.path, None),
                   hdr.fields.get(HeaderFields.interface, None),
                   hdr.fields.get(HeaderFields.member, None)
                  )
            callbacks = self._method_callbacks.get(key, None)
            if callbacks is not None:
                for cb in callbacks:
                    self._dispatch_cb(cb, msg.body, hdr)
                return

        # Method returns & errors:
        reply_serial = hdr.fields.get(HeaderFields.reply_serial, -1)
        reply_handle = self._reply_handles.pop(reply_serial, None)

        if reply_handle is not None:
            if hdr.message_type is MessageType.method_return:
                reply_handle.set_result(msg.body)
                return
            elif hdr.message_type is MessageType.error:
                reply_handle.set_exception(DBusErrorResponse(msg))
                return

        self._unknown_cb(msg)
