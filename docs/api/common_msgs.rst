Common messages
===============

.. currentmodule:: jeepney

These classes are *message generators*. Wrap them in a :ref:`Proxy
<msggen_proxies>` class to actually send the messages as well.

.. autoclass:: Properties
   :members:

.. autoclass:: Introspectable
   :members:

.. module:: jeepney.bus_messages

.. autoclass:: DBus
   :members:
   :undoc-members:

.. autoclass:: Monitoring
   :members:
